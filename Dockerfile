FROM migrate/migrate

COPY . /migrator
WORKDIR migrator
RUN chmod +x migrate.sh

ENTRYPOINT []
CMD ./migrate.sh
