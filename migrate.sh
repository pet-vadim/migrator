#!/bin/sh
migrate -path=migrations -database "postgres://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_TABLE}?sslmode=disable" up
