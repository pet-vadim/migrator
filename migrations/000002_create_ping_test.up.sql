CREATE TABLE ping_test
(
    id               uuid PRIMARY KEY NOT NULL UNIQUE,
    name             varchar(255)     NOT NULL,
    url              varchar(8000)    NOT NULL,
    wont_status_code int,
    fail_msg         varchar(4000)
);