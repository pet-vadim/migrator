ALTER TABLE ping_test ADD user_id uuid REFERENCES users(id) ON DELETE CASCADE;
ALTER TABLE ping_test ADD timeout_seconds int NOT NULL DEFAULT 60;